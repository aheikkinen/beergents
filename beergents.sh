#!/bin/bash

sCommand=${1:-"dev"}

sVM="dev"
sUser="vmadmin"
sBaseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

case ${sCommand} in
  create)
    echo -e "\n======================\nCreate virtual machine\n======================\n"
    ansible-playbook ${sBaseDir}/local/vm/main.yaml
    ;;
  provision)
    echo -e "\n===========================\nProvision website resources\n===========================\n"
    ansible-playbook ${sBaseDir}/ansible/playbook.yaml -i ${sBaseDir}/ansible/inventory.yaml -l local
    ;;
  ssh)
    sIpAddress=$(multipass info ${sVM} --format=json | jq -r .info.dev.ipv4[])
    ssh -i ${sBaseDir}/local/vm/generated/user_key ${sUser}@${sIpAddress}
    ;;
  serve)
    echo -e "\n=============\nServe website\n=============\n"
    node ${sBaseDir}/local/server/server.js
    ;;
  *)
    echo "Usage: ./beergents.sh COMMAND"
    echo "COMMANDS:"
    echo "  create        Create virtual machine"
    echo "  provision     Provision website resources"
    echo "  ssh           Shell connection to virtual machine"
    echo "  serve         Server website"
    ;;
esac
