var serverPort=9000;
	
var express = require('express');
var bodyParser = require('body-parser')
const path = require('path')

var app = express();
app.use(bodyParser.json());

let webDir = path.join(__dirname, "../../app/website")

app.use(express.static(webDir));

app.listen(serverPort);
console.log('Server listening on http://localhost:'+serverPort);
console.log('Distributing site from: '+webDir);
